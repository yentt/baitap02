import React from 'react';
import {
  SafeAreaView,
  TouchableOpacity,
  TouchableOpacityProps,
  StyleSheet,
  Text,
  ViewStyle,
  TextStyle,
  View,
  TextInput,
} from 'react-native';

type Props = TouchableOpacityProps & {
  buttonStyle?: ViewStyle;
  textStyle?: TextStyle;
  buttonText?: string;
  customStyles: ViewStyle;
};

const MyButton: React.FC<Props> = ({
  textStyle = styles.textStyle,
  buttonStyle = styles.button,
  buttonText,
  customStyles,
}) => {
  return (
    <TouchableOpacity style={[buttonStyle, customStyles]}>
      <Text style={textStyle}>{buttonText}</Text>
    </TouchableOpacity>
  );
};

const App = () => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.paddingContent}>
        <Text style={styles.titleStyle}>Yolo System</Text>
      </View>
      <View style={styles.paddingContent}>
        <TextInput style={styles.input} placeholder="Tên đăng nhập" />
      </View>
      <View style={styles.paddingContent}>
        <TextInput
          secureTextEntry={true}
          style={styles.input}
          placeholder="Mật khẩu"
        />
      </View>
      <View style={styles.paddingContent}>
        <MyButton
          customStyles={{backgroundColor: 'purple'}}
          buttonText="Login"
        />
      </View>
      <View style={styles.paddingContent}>
        <Text style={styles.titleStyle}>Or</Text>
      </View>
      <View style={styles.paddingContent}>
        <MyButton
          customStyles={{backgroundColor: 'green'}}
          buttonText="Facebook"
        />
      </View>
      <View style={styles.paddingContent}>
        <MyButton customStyles={{backgroundColor: 'red'}} buttonText="Google" />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  paddingContent: {
    padding: 5,
  },
  button: {
    padding: 10,
    marginHorizontal: 10,
    borderRadius: 20,
    borderColor: 'black',
    borderWidth: 1,
  },
  textStyle: {
    fontWeight: '700',
    textAlign: 'center',
    color: 'black',
  },
  titleStyle: {
    color: 'green',
    fontWeight: 'bold',
    fontSize: 50,
    textAlignVertical: 'center',
    textAlign: 'center',
  },
  input: {
    height: 40,
    marginHorizontal: 10,
    borderWidth: 1,
    borderRadius: 10,
  },
});

export default App;
